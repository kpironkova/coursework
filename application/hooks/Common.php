<?

class Common
{
    private $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
    }

    public function user_info()
    {
        if ( is_logged_in() )
            $this->CI->data['user_info'] = $this->CI->user_model->get_info();
    }

    public function media_genres()
    {
        $this->CI->data['media_genres'] = $this->CI->media_model->get_media_genres();
    }
}

?>