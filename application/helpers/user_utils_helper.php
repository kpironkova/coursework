<?php
	function is_logged_in(){
		//get instance, access the CI superobject
		$CI = & get_instance();
		// getting is_logged_in variable from session
		$is_logged_in = $CI->session->userdata('is_logged_in');
		// if exists
		if( $is_logged_in) { // 
			return TRUE;
		}

		return FALSE;  
	}