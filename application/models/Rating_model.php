<?php
    
    class Rating_model extends CI_Model {
        
        private $table = 'media_rating';
        
        public function set_rating($media_id, $rate) {
            $this->db->insert($this->table, [
                'media_id' => $media_id,
                'user_id' => $this->session->userdata('user_id'),
                'rate' => $rate,
            ]);
        }
        
        public function check_rated($media_id) {
            return $this->db->select('*')->where( ['media_id' => $media_id, 'user_id' => $this->session->userdata('user_id')] )->get( $this->table )->row('rate');
        }
        
        public function get_avg_rate($media_id) {
            return $this->db->select_avg('rate')->where( 'media_id', $media_id )->get( $this->table )->row('rate');
        }
        
        public function get_vote_count($media_id) {
            return $this->db->select('id')->where('media_id', $media_id)->get($this->table)->num_rows();
        }
        
        public function get_best_rated($limit = 6) {
           return $this->db->query('SELECT media.*, AVG(rate) as rate FROM media_rating INNER JOIN media ON media_rating.media_id=media.id GROUP BY media.id ORDER BY rate DESC LIMIT ?', array($limit))->result();
        }
    }