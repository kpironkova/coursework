<?php

class Comments_model extends CI_Model {
    private $table_comments = 'comments';
    private $columns = 'id, user_id, data';
    
    public function get_comments($media_id) {
        return $this->db->query("SELECT comments.data, users.username FROM comments INNER JOIN users ON comments.user_id = users.id WHERE media_id = ? ORDER BY comments.id DESC", $media_id)->result();
    }
    
    public function get_comments_by_user_id($user_id)
    {
        return $this->db->query("SELECT comments.data, users.username, comments.media_id FROM comments INNER JOIN users ON comments.user_id = users.id WHERE user_id = ? ORDER BY comments.id DESC", $user_id)->result();
    }
    
    public function post_comment($media_id) {
        $data = [
            'user_id' => $this->session->userdata('user_id'),
            'data' => $this->input->post('comment'),
            'media_id' => $media_id,
            'ip' => $_SERVER['REMOTE_ADDR']
        ];
        
        $this->db->insert($this->table_comments, $data);
    }
    
}
