<?php

class Watched_model extends CI_Model
{
     private $table = 'users_watched';
     private $columns = 'id, media_id, date_watched';
     
     public function exist_in_watched($media_id){
        $result = $this->db->where([
            'media_id' => $media_id,
            'user_id' => $this->session->userdata('user_id')
        ])->get($this->table);
        if($result->num_rows() == 1)
            return true;
        else 
            return false;
     }
     
     public function get_all(){
        return $this->db->select($this->columns)->where('user_id', $this->session->userdata('user_id'))->get($this->table)->result();
     }
     
     public function add_to_watched($media_id){
         $data = [
            'media_id' => $media_id,
            'user_id' => $this->session->userdata('user_id'),
        ];
        
        $this->db->insert($this->table, $data);       
     }
     
      public function delete_from_watched($media_id) {
        $this->db->delete($this->table, ['media_id' => $media_id, 'user_id' => $this->session->userdata('user_id')]);
      }
     
     public function get_chart_data()
     {
        $genres = $this->db->query('SELECT COUNT(*) as num, genre FROM users_watched uw
                            INNER JOIN media_genres_assigned mga USING(media_id)
                            INNER JOIN media_genres mg ON mg.id = mga.genre_id
                            WHERE user_id = ?
                            GROUP BY genre_id', $this->session->userdata('user_id'))->result();
                      
        $total = 0;
              
        foreach($genres as $genre)
            $total += $genre->num;
            
        foreach($genres as $index => $genre)
            $genres[$index]->percent = round(($genre->num/$total)*100);
            
        return $genres;
     }
     
     public function sum_watched_time() {
        return $this->db->select_sum('length')->from($this->table)->join('media', 'media.id = users_watched.media_id')->where('user_id', $this->session->userdata('user_id'))->get()->row('length');
     }     
}