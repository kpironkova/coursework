<?php

class Watchlist_model extends CI_Model {
     private $table= 'users_watchlist';
     private $columns = 'id, media_id, date_added';
     
     public function exist_in_watchlist($media_id){
        $result = $this->db->where([
            'media_id' => $media_id,
            'user_id' => $this->session->userdata('user_id'),
        ])->get($this->table);

        if ($result->num_rows() == 1)
            return true;
        else 
            return false;
    }
     
     public function get_all(){
        return $this->db->select($this->columns)->where('user_id', $this->session->userdata('user_id'))->get($this->table)->result();
     }
     
    public function add_to_watchlist($media_id) {
        $data = [
            'media_id' => $media_id,
            'user_id' => $this->session->userdata('user_id'),
        ];
        
        $this->db->insert($this->table, $data);
    }
    
    public function delete_from_watchlist($media_id) {
        $this->db->delete($this->table, ['media_id' => $media_id, 'user_id' => $this->session->userdata('user_id')]);
    }
    
}