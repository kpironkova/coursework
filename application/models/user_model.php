<?php

class User_model extends CI_Model
{
    private $table = 'users';
    private $columns = 'id, username, email, first_name, last_name';

    public function register()
    {
        $data = array(
            'email' => $this->input->post('email_reg'),
            'username' => $this->input->post('username_reg'),
            'password' => $this->input->post('password'),
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'));

        $this->db->insert($this->table, $data);
    }

    public function login()
    {
        $result = $this->db->select($this->columns)->where(['email' => $this->input->
            post('email'), 'password' => $this->input->post('password')])->get($this->table);

        if ($result->num_rows() == 1) {
            $this->session->set_userdata(['user_id' => $result->row()->id, 'is_logged_in' => true]);
            return true; // Трябва да има return true, защото иначе дори и при валиден логин връща грешка.
        } else
            return false;
    }

    public function get_info()
    {
        return $this->db->select($this->columns)->where(['id' => $this->session->
            userdata('user_id')])->get($this->table)->first_row();
    }

    public function check_password()
    {
        $db_password = $this->db->select('password')->where(['id' => $this->session->
            userdata('user_id')])->get($this->table)->row('password');
        if ($db_password == $this->input->post('password_current')) {
            return true;
        } else
            return false;
    }

    public function set_personal_info()
    {

        if (!empty($this->input->post('username_new')))
            $data['username'] = $this->input->post('username_new');
        if (!empty($this->input->post('first_name')))
            $data['first_name'] = $this->input->post('first_name');
        if (!empty($this->input->post('last_name')))
            $data['last_name'] = $this->input->post('last_name');

        $this->db->update($this->table, $data, ['id' => $this->session->userdata('user_id')]);
    }

    public function set_email()
    {
        $this->db->update($this->table, ['email' => $this->input->post('email_reg')], ['id' =>
            $this->session->userdata('user_id')]);
    }

    public function set_password()
    {
        $this->db->update($this->table, ['password' => $this->input->post('password')], ['id' =>
            $this->session->userdata('user_id')]);
    }
    
}
