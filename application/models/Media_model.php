<?php

class Media_model extends CI_Model
{
    private $table_media = 'media';
    private $table_genres = 'media_genres';
    private $table_media_genres_assigned = 'media_genres_assigned';
    private $columns = 'media.id, media.title, media.year, media.poster';

    private $genre_id = '';
    private $year = '';

    public function set_genre($id)
    {
        $this->genre_id = $id;
    }

    public function set_year($year)
    {
        $this->year = $year;
    }

    public function set_type($type)
    {
        $this->type = $type;
    }

    public function exists_genre($id)
    {
        $result = $this->db->where('id', $id)->get($this->table_genres);

        if ($result->num_rows() == 1)
            return true;
    }

    public function exists_media($id)
    {
        $result = $this->db->where('id', $id)->get($this->table_media);

        if ($result->num_rows() == 1)
            return true;
    }

    public function get_all($limit = null)
    {
        if (!empty($this->genre_id))
        {
            $this->db->join('media_genres_assigned', 'media_genres_assigned.media_id = media.id');
            $this->db->where('media_genres_assigned.genre_id', $this->genre_id);
        }
            

        if (!empty($this->year))
            $this->db->where('year', $this->year);
            

        if (!empty($this->type))
            $this->db->where('type', $this->type);

        return $this->db->select($this->columns)->order_by('id', 'desc')->get($this->table_media, $limit)->result();
    }

    public function get_media_genres()
    {
        return $this->db->select('id, genre')->get($this->table_genres)->result();
    }

    public function get_movie($id)
    {
        $movie = $this->db->where('id', $id)->get($this->table_media)->row();
        $movie->genres = $this->db->query('SELECT media_genres.* FROM media_genres INNER JOIN media_genres_assigned ON media_genres.id = media_genres_assigned.genre_id WHERE media_genres_assigned.media_id = ?', $id)->result();

        return $movie;
    }
    
    public function get_related_movies($id, $limit)
    {
        return $this->db->query("SELECT *, count(*) as relation_score FROM media_genres_assigned INNER JOIN media ON media.id = media_genres_assigned.media_id 
            WHERE media.id != ?
            AND genre_id IN (
                SELECT media_genres.id FROM media_genres INNER JOIN media_genres_assigned ON media_genres.id = media_genres_assigned.genre_id WHERE media_genres_assigned.media_id = ?
            )  GROUP BY media.id  HAVING relation_score > 1 ORDER BY relation_score DESC LIMIT ?", array($id, $id, $limit))->result();
    }
    
    public function get_related_movies_all($limit)
    {
        return $this->db->query("SELECT *, count(*) as relation_score FROM media_genres_assigned INNER JOIN media ON media.id = media_genres_assigned.media_id 
            WHERE media.id NOT IN (
                SELECT media_id FROM users_watched WHERE user_id = ?
            )
            AND genre_id IN (
                SELECT media_genres.id FROM media_genres INNER JOIN media_genres_assigned ON media_genres.id = media_genres_assigned.genre_id 
                WHERE media_genres_assigned.media_id IN (
                    SELECT media_id FROM users_watched WHERE user_id = ?
                )
            )  GROUP BY media.id  HAVING relation_score > 1 ORDER BY relation_score DESC LIMIT ?", array($this->session->userdata('user_id'), $this->session->userdata('user_id'), $limit))->result();
    }
    
    public function get_links($id)
    {
        return $this->db->where('media_id', $id)->get('media_links')->result();
    }
    
    public function get_poster($id){
        return $this->db->select('poster')->where('id', $id)->get($this->table_media)->row()->poster;
    }

    public function get_title($id){
        return $this->db->select('title')->where('id', $id)->get($this->table_media)->row()->title;
    }

    public function get_year($id){
        return $this->db->select('year')->where('id', $id)->get($this->table_media)->row()->year;
    }



}
