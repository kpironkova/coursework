<?php

class Crawler_model extends CI_Model {

    public function zamunda_parse_title( $string, $tv = false )
    {
        if($tv)
        {
            preg_match( '/(.+)S(.{2})E(.{2})/', $string, $match );
            foreach ( $match as &$item )
                $item = trim( $item );
    
            if ( count( $match ) != 4 )
                return false;
            else
                return $match;
        }
        else
        {
            preg_match( '/(.+)\/(?:.*)\((.+)\)/', $string, $match );
            foreach ( $match as &$item )
                $item = trim( $item );
    
            if ( count( $match ) != 3 )
                return false;
            else
                return $match;
        }
    }

    public function zamunda_parse_size( $string )
    {
        preg_match( '/Size\:(.+)Status/s', $string, $match );
               
        foreach ( $match as &$item )
            $item = trim( $item );

        if ( count( $match ) != 2 )
            return false;
        else
            return $match[1];
    }
    
    public function get_movie_info( $title, $year )
    {
        $endpoint = 'http://www.omdbapi.com/?t=' . urlencode( $title ) . '&y=' . $year . '&plot=full&r=json';
        $raw_json = file_get_contents( $endpoint );
        return json_decode( $raw_json );
    }
    
    public function get_tv_info( $title, $episode )
    {
        $endpoint = 'http://www.omdbapi.com/?t=' . urlencode( $title ) . '&plot=full&r=json&season='.$episode[0].'&episode='.$episode[1];
        $raw_json = file_get_contents( $endpoint );
        $episode_info = json_decode($raw_json);
        
        if( 'True' != $episode_info->Response )
            return false;
        
        $endpoint = 'http://www.omdbapi.com/?i='.$episode_info->seriesID.'&plot=full&r=json';
        $raw_json = file_get_contents( $endpoint );

        return json_decode( $raw_json );
    }
    
    public function insert($array, $tv = false)
    {
        $this->load->library('image_lib');
        
        foreach($array as $movie)
        {
            $movieID = $this->_movie_exists($movie->Title, $movie->Year, (int) $movie->Runtime, $tv);
            if($movieID != NULL)
            {
                echo 'exists: '.$movieID.' '.$movie->Title.'<br />';
                $this->_insert_link($movieID, $movie->Link, $movie->LinkType, $movie->Size);
            }
            else
            {
                $movieID = $this->_insert_media($movie, $tv);
                if($movieID)
                {
                    echo 'new: '.$movieID.' '.$movie->Title.'<br />';
                    $this->_insert_link($movieID, $movie->Link, $movie->LinkType, $movie->Size);
                }
            }
        }
    }
    
    private function _movie_exists($title, $year, $length, $tv)
    {
       return @$this->db->select('id')->where(['title'=>$title, 'year'=>$year, 'length'=>$length, 'type'=>($tv?2:1)])->get('media')->row()->id;
    }
    
    private function _insert_link($media_id, $link, $type, $size)
    {       
        $link_db = $this->db->select('id')->where(['link'=>$link])->get('media_links');
        if($link_db->num_rows() != 1)
            $this->db->insert('media_links', ['media_id'=>$media_id, 'link'=>$link, 'type'=>$type, 'size'=>$size]);
    }
    
    private function _get_genre_id($genre_str)
    {
        $genre_str = trim($genre_str);
        
        $genre = $this->db->select('id')->where(['genre'=>$genre_str])->get('media_genres');
        if($genre->num_rows() == 1)
            return $genre->row()->id;
        else
        {
            $this->db->insert('media_genres', ['genre'=>$genre_str]);
            return $this->db->insert_id();
        }
    }
    
    private function _insert_media($movie, $tv)
    {
        $data = array('title'=>$movie->Title, 'year'=>$movie->Year, 'length'=>(int)$movie->Runtime, 'description'=>$movie->Plot, 'type'=>($tv?2:1), 'director'=>$movie->Director, 'actors'=>$movie->Actors);
        
        $poster = $this->_download_poster($movie->Year, $movie->Poster);
        if(!$poster)
            return;
        
        $data['poster'] = $poster;
        
        $this->db->insert('media', $data);
        
        $media_id = $this->db->insert_id(); 
        
        foreach($movie->Genre as $genre)
        {
            $genre_id = $this->_get_genre_id($genre);
            $this->db->insert('media_genres_assigned', ['genre_id'=>$genre_id, 'media_id'=>$media_id]);
        }
        
        return $media_id;
    }
    
    private function _download_poster($year, $url)
    {             
        $year = (int) $year; // Правим това заради сериалите
        $ext = @end(explode('.', $url));
        if(!$ext)
            $ext = 'jpg';
            
        $filename = md5($year.$url).'.'.$ext;
        
        $base_dir = $_SERVER['DOCUMENT_ROOT'].'/assets/posters/';
        if(!is_dir($base_dir.$year))
        {
            mkdir($base_dir.$year);
            mkdir($base_dir.$year.'/o');
            mkdir($base_dir.$year.'/165x245');
            mkdir($base_dir.$year.'/350x525');
        }

        $base_dir .= $year.'/';
        
        if(is_file($base_dir.'o/'.$filename))
            return $filename;
        else
        {
            $source = @file_get_contents($url);
            
            if(!$source)
                return;
                
            if(file_put_contents($base_dir.'o/'.$filename, $source))
            {
                $this->_fit_image($base_dir.'o/'.$filename, $base_dir.'165x245/'.$filename, 165, 245);
                $this->_fit_image($base_dir.'o/'.$filename, $base_dir.'350x525/'.$filename, 350, 525);
                
                return $filename;
            }
        }
    }
    
    function _fit_image( $oldPath, $newPath, $width, $height, $dynamic = false )
    {
        $config['source_image'] = $oldPath;
        $config['new_image'] = $newPath;
        $config['width'] = $width;
        $config['height'] = $height;
        $config['dynamic_output'] = $dynamic;
        $config['master_dim'] = 'auto';

        $this->image_lib->initialize( $config );
        $this->image_lib->fit();
        $this->image_lib->clear();
    }
}
