<?php

class Layout extends CI_Model
{

    function render($page)
    {

        $this->load->view('includes/header', $this->data);
        $this->load->view('pages/' . $page );
        $this->load->view('includes/footer', $this->data );
    }

    function show_message( $alert = '' )
    {
        
        if ( ! is_array( $alert ) && ! $this->session->flashdata( 'alert' ) )
            return '';

        if ( null !== $this->session->flashdata( 'alert' ) )
            $alert = $this->session->flashdata( 'alert' );


        if ( $alert )
            $this->load->view('includes/common/alert', ['text' => $alert[1], 'class' => $alert[0]] );
    }
}
