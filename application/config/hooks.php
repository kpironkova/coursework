<?php

defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/

$hook['post_controller_constructor'][] = array(
    'class' => 'Common',
    'function' => 'user_info',
    'filename' => 'Common.php',
    'filepath' => 'hooks',
    'params' => array() );

$hook['post_controller_constructor'][] = array(
    'class' => 'Common',
    'function' => 'media_genres',
    'filename' => 'Common.php',
    'filepath' => 'hooks',
    'params' => array() );