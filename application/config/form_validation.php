<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	$email = [
		'field' => 'email',
		'label' => 'Email',
		'rules' => 'trim|required|valid_email'
	];

	$email_register = [
            'field' => 'email_reg',
            'label' => 'Email',
            'rules' => 'trim|required|max_length[100]|min_length[5]|valid_email|is_unique[users.email]'
    ];

    $email_confirm = [
        'field' => 'emailconf',
        'label' => 'Confirm email',
        'rules' => 'trim|required|max_length[100]|min_length[5]|valid_email|matches[email_reg]'
    ];

    $username = [
    	'field' => 'username',
    	'label' => 'Username',
    	'rules' => 'trim|required|max_length[100]|min_length[5]'
    ];

    $username_register = [
    	'field' => 'username_reg',
    	'label' => 'Username',
    	'rules' => 'trim|required|max_length[100]|min_length[3]|is_unique[users.username]'
    ];

    $username_new = [
        'field' => 'username_new',
        'label' => 'Username',
        'rules' => 'trim|max_length[100]|min_length[3]|is_unique[users.username]'
    ];

    $password_current = [
        'field' => 'password_current',
        'label' => 'password',
        'rules' => 'trim|required|max_length[30]|min_length[4]|sha1'
    ];

    $password = [
        'field' => 'password',
        'label' => 'Password',
        'rules' => 'trim|required|max_length[30]|min_length[4]|sha1'
    ];

    $password_confirm = [
        'field' => 'passconf',
        'label' => 'Confirm password',
        'rules' => 'trim|required|max_length[30]|min_length[4]|sha1|matches[password]'
    ];

    $first_name = [
        'field' => 'first_name',
        'label' => 'First name',
        'rules' => 'trim'
    ];

    $last_name = [
        'field' => 'last_name',
        'label' => 'Last name',
        'rules' => 'trim'
    ];
    
    $comment = [
        'field' => 'comment',
        'label' => 'Comment',
        'rules' => 'trim|required'
        
    ];

	$config = [
		'users/login' => [$email,$password],
		'users/register' => [$username_register, $email_register, $password, $password_confirm, $first_name, $last_name],
        'users/settings' => [$username_new, $first_name, $last_name],
        'users/set_email' => [$email, $email_register, $email_confirm],
        'users/set_password' => [$password_current, $password, $password_confirm],
        'comments/post_comment' => [$comment]
	];