<div role="main" class="main">
				<div class="slider-container">
					<div class="slider" id="revolutionSlider" data-plugin-revolution-slider data-plugin-options='{"startheight": 500}'>
						<ul>
							<li data-transition="fade" data-slotamount="13" data-masterspeed="300" >
								<iframe class="slide-show-video" src="https://player.vimeo.com/video/91160492"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							</li>
							<li data-transition="fade" data-slotamount="13" data-masterspeed="300" >
								<iframe class="slide-show-video" src="https://player.vimeo.com/video/140587787"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							</li>
						</ul>
					</div>
				</div>
				<div class="home-intro" id="home-intro">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<p>
									The fastest way to wacth <em>Trailers</em> of the latest movies !
								</p>
							</div>
							<div class="col-md-4">
								<div class="get-started">
									<a href="#" class="btn btn-lg btn-primary">Watch more trailers</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="container">
					<div class="row center">
						<div class="col-md-12">
							<h1 class="mb-sm word-rotator-title">
								Latest movies	
							</h1>
						</div>
					</div>
					<div class="row center">
                        <ul class="portfolio-list">
                            <?php foreach($latest_movies as $movie): ?>
                            	<li class="col-md-2 col-sm-6 col-xs-12">
									<div class="portfolio-item">
										<a href="<?= base_url("catalog/view/{$movie->id}") ?>">
											<span class="thumb-info">
												<span class="thumb-info-wrapper">
													<img src="<?=base_url()?>assets/posters/<?=$movie->year?>/165x245/<?=$movie->poster?>" class="img-responsive" alt="">
                                                    <span class="thumb-info-title">
														<span class="thumb-info-inner"><?= $movie->title ?></span>
														<span class="thumb-info-type"><?= $movie->year ?></span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</li>  
                            <?php endforeach; ?>
                        </ul>
					</div>
				</div>
				
				<div class="home-concept">
					<div class="container">
						<div class="row center">
						</div>
					</div>
				</div>

				<div class="container">
					<hr class="tall">
					<div class="row center">
						<div class="col-md-12">
							<h2 class="mb-sm word-rotator-title">
								Best Rated
							</h2>
						</div>
					</div>
				
					<div class="row center">
						<div class="owl-carousel" data-plugin-options='{"items": 6, "autoplay": true, "autoplayTimeout": 2000}'>
                            <? foreach($best_rated_movies as $movie): ?>
									<div class="portfolio-item" style="margin-right:15px;">
										<a href="<?= base_url("catalog/view/{$movie->id}") ?>">
											<span class="thumb-info">
												<span class="thumb-info-wrapper">
													<img src="<?=base_url()?>assets/posters/<?=$movie->year?>/165x245/<?=$movie->poster?>" class="img-responsive" alt="">
                                                    <span class="thumb-info-title">
														<span class="thumb-info-inner"><?= $movie->title ?></span>
														<span class="thumb-info-type">Rating: <?= round($movie->rate, 2) ?></span>
													</span>
												</span>
											</span>
										</a>
									</div>
                            <? endforeach; ?>
						</div>
					</div>
				</div>
			</div>