
	<div class="container">

		<h2>Recommended movies &amp; TV Series for you</h2>

		<hr />

		<div class="row">

			<ul class="portfolio-list">
                <? foreach($movies as $movie): ?>
				<li class="col-md-2 col-sm-3 col-xs-3 isotope-item websites">
					<div class="portfolio-item">
						<a href="<?=site_url('catalog/view/'.$movie->id)?>">
							<span class="thumb-info">
								<span class="thumb-info-wrapper">
									<img src="<?=base_url()?>assets/posters/<?=$movie->year?>/165x245/<?=$movie->poster?>" class="img-responsive" alt="">
									<span class="thumb-info-title">
										<span class="thumb-info-inner"><?=text($movie->title)?></span>
                                        <? /*
										<span class="thumb-info-type">Website</span>
                                        */ ?>
									</span>
									<span class="thumb-info-action">
										<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
									</span>
								</span>
							</span>
						</a>
					</div>
				</li>
                <? endforeach; ?>
			</ul>
		</div>

	</div>

</div>