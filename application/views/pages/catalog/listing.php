
	<div class="container">

		<h2>Catalogue <?=((null !== $this->uri->segment(2) && $this->uri->segment(2) == 'tv')?'TV Series':'Movies')?></h2>
		<div class="dropdown">
			<ul class="nav nav-pills sort-source" >
				<li class="active"><a href="<?=base_url('catalog')?>">Show All</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Genres<? 
                    foreach($media_genres as $genre):
                        if (isset($_GET['genre']) && ($_GET['genre'] == $genre->id))
                            echo ': '.text($genre->genre);
                    endforeach; 
                    ?>
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?=current_url()?>">All</a></li>
                        <? foreach($media_genres as $genre): ?>
    					<li class="<?php if (isset($_GET['genre']) && ($_GET['genre'] == $genre->id)) echo "active"; ?>"><a href="<?=current_url().'/?genre='.$genre->id?>"><?=text($genre->genre)?></a></li>
                        <? endforeach; ?>
                    </ul>
                </li>
			</ul>
		</div>

		<hr />

		<div class="row">

			<ul class="portfolio-list">
                <? foreach($movies as $movie): ?>
				<li class="col-md-2 col-sm-3 col-xs-3 isotope-item websites">
					<div class="portfolio-item">
						<a href="<?=site_url('catalog/view/'.$movie->id)?>">
							<span class="thumb-info">
								<span class="thumb-info-wrapper">
									<img src="<?=base_url()?>assets/posters/<?=$movie->year?>/165x245/<?=$movie->poster?>" class="img-responsive" alt="">
									<span class="thumb-info-title">
										<span class="thumb-info-inner"><?=text($movie->title)?></span>
                                        <? /*
										<span class="thumb-info-type">Website</span>
                                        */ ?>
									</span>
									<span class="thumb-info-action">
										<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
									</span>
								</span>
							</span>
						</a>
					</div>
				</li>
                <? endforeach; ?>
			</ul>
		</div>

	</div>

</div>