	<div class="container">

				<div class="row">
					<div class="col-md-12">
						<div class="portfolio-title">
							<div class="row">
								<div class="portfolio-nav-all col-md-1">
									<a href="<?=site_url('catalog')?>" data-tooltip data-original-title="Back to catalogue"><i class="fa fa-th"></i></a>
								</div>
								<div class="col-md-10 center">
									<h2 class="mb-none"><?=text($movie->title)?></h2>
								</div>
								<div class="portfolio-nav col-md-1">
									<a href="portfolio-single-project.html" class="portfolio-nav-prev" data-tooltip data-original-title="Previous"><i class="fa fa-chevron-left"></i></a>
									<a href="portfolio-single-project.html" class="portfolio-nav-next" data-tooltip data-original-title="Next"><i class="fa fa-chevron-right"></i></a>
								</div>
							</div>
						</div>

						<hr class="tall" />
					</div>
				</div>

				<div class="row">
					<div class="col-md-4">
							<div>
								<span class="img-thumbnail">
									<img alt="" class="img-responsive" src="<?=base_url()?>assets/posters/<?=$movie->year?>/350x525/<?=$movie->poster?>" />
								</span>
							</div>	
                            <?php if($in_watched == FALSE): ?> 
                                <a href="<?=site_url("watched/add_to_watched/{$movie->id}")?>" class="btn btn-primary" style="width: 100%;">Mark as watched</a>
                             <?php else: ?>
                                <a href="<?=site_url("watched/delete_from_watched/{$movie->id}")?>" class="btn btn-danger" style="width: 100%;">Unmark as watched</a>
                             <?php endif; ?>
                    </div>		 

					<div class="col-md-8">
                        <?php $this->layout->show_message(); ?>
						<div class="portfolio-info">
							<div class="row">
								<div class="col-md-12 center">
									<ul>
										<li>
											<a href="#" data-tooltip data-original-title="Like"><i class="fa fa-heart"></i>14</a>
										</li>
										<li>
											<i class="fa fa-calendar"></i><?=text($movie->year)?>
										</li>
										<li>
											<i class="fa fa-tags"></i> <a href="#"><?=text($movie->length)?> min</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
                        
						<h4 class="heading-primary"><?=text($movie->title)?></h4>
                        <?= form_open(current_url()); ?>
                            <div class="star-rating">
                              <div class="star-rating__wrap">
                                <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5" <?php if ($rating == 5) echo 'checked'; ?>>
                                <label class="star-rating__ico fa fa-star-o fa-2x" for="star-rating-5" title="5 out of 5 stars"></label>
                                <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4" <?php if ($rating == 4) echo 'checked'; ?>>
                                <label class="star-rating__ico fa fa-star-o fa-2x" for="star-rating-4" title="4 out of 5 stars"></label>
                                <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3" <?php if ($rating == 3) echo 'checked'; ?>>
                                <label class="star-rating__ico fa fa-star-o fa-2x" for="star-rating-3" title="3 out of 5 stars"></label>
                                <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2" <?php if ($rating == 2) echo 'checked'; ?>>
                                <label class="star-rating__ico fa fa-star-o fa-2x" for="star-rating-2" title="2 out of 5 stars"></label>
                                <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1" <?php if ($rating == 1) echo 'checked'; ?>>
                                <label class="star-rating__ico fa fa-star-o fa-2x" for="star-rating-1" title="1 out of 5 stars"></label>
                                <div class="vote" >
                                    <?php if ($rating == null): ?>
                                        <button type="submit" class="btn btn-primary" name="vote">Vote</button>
                                    <?php endif; ?>
                                </div>
                                <span class="label label-warning">AVG: <?= round($total_rating) ?>(<?= $vote_count ?> users voted)</span>
                              </div>
                            </div>
                            
                        <?= form_close(); ?>

						<p class="mt-xlg"><?=text($movie->description)?></p>

						<ul class="portfolio-details">
							<li>
								<p><strong>Actors:</strong></p>
								<p><?=$movie->actors?></p>
							</li>
							<li>
								<p><strong>Director:</strong></p>
								<p><?=$movie->director?></p>
							</li>
							<li>
								<p><strong>Genre:</strong></p>

								<ul class="list list-inline list-icons">
                                    <?php foreach($movie->genres as $genre): ?>
									<li><i class="fa fa-check-circle"></i><a href="<?=site_url('catalog?genre='.$genre->id)?>"><?=$genre->genre?></a></li>
                                    <?php endforeach; ?>
								</ul>
							</li>
						</ul>
                            <div class="dropdown" style="float:left;margin-right:4px;">
                                  <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownLinks" data-toggle="dropdown">
                                    <i class="fa fa-download"></i> Download</span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownLinks">
                                    <?php foreach($links as $link): ?>
                                    <li><a href="<?=$link->link?>" target="_blank"><?=$link->type?> - <?=$link->size?></a></li>
                                    <?php endforeach; ?>
                                  </ul>
                            </div>
                            <? /*
							<a href="#" class="btn btn-primary btn-icon"><i class="fa fa-play"></i>Watch trailer</a>
                            */ ?>
                            <?php if($in_watchlist == FALSE): ?> 
							<a href="<?php echo site_url("watchlist/add_to_watchlist/{$movie->id}");?>" class="btn btn-success btn-icon"><i class="fa fa-plus"></i>Add to watchlist</a> 
                            <?php else: ?>
                            <a href="<?php echo site_url("watchlist/delete_from_watchlist/{$movie->id}");?>" class="btn btn-danger btn-icon"><i class="fa fa-trash"></i>Remove from watchlist</a> 
                            <?php endif; ?>
                    </div>
				</div>

				<div class="row">
					<div class="col-md-12">

						<hr class="tall">

						<h4 class="mb-md text-uppercase">Related <strong>Movies</strong></h4>
							
						<div class="row">
							<ul class="portfolio-list">
                            
                            <?php $i = 0; ?>
                            <?php foreach($related_movies as $movie_): ?>
                            	<li class="col-md-2 col-sm-6 col-xs-12">
									<div class="portfolio-item">
										<a href="<?= base_url("catalog/view/{$movie_->id}") ?>">
											<span class="thumb-info">
												<span class="thumb-info-wrapper">
													<img src="<?=base_url()?>assets/posters/<?=$movie_->year?>/165x245/<?=$movie_->poster?>" class="img-responsive" alt="">
                                                    <span class="thumb-info-title">
														<span class="thumb-info-inner"><?= $movie_->title ?></span>
														<span class="thumb-info-type"><?= $movie_->year ?></span>
													</span>
												</span>
											</span>
										</a>
									</div>
								</li>  
                            <?php if (++$i == 4) break; ?>
                            <?php endforeach; ?>
							</ul>

						</div>
					</div>
				</div>
            
				<h3 class="heading-primary"><i class="fa fa-comments"></i> Comments (<?= $comments_count; ?>)</h3>
                <div class="col-md-6">
                <?php if (is_logged_in()): ?>
                    <?=form_open(current_url()); ?>
                    
					<div class="row">
						<div class="form-group">
							<div class="col-md-12">
                                <textarea rows="5" class="form-control" name="comment" placeholder="Write your comment!"></textarea>
                                <?php echo form_error('comment', '<span class="small red"> *', '</span>'); ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							 <input value="Post Comment" class="btn btn-primary btn-lg" name="submit_comment" data-loading-text="Loading..." type="submit" />
						</div>
					</div>

                    
                    <?= form_close(); ?>
                <?php else: ?>
                    <div class="alert alert-danger">Please <a href="<?= base_url('users/login') ?>">login</a> to comment the movie.</div>
                <?php endif; ?>
                </div>	
				<div class="col-md-6">
                    <?php foreach($comments as $comment): ?>
					<div class="testimonial testimonial-style-3">
						<blockquote>
							<p><?= $comment->data; ?></p>
						</blockquote>
						<div class="testimonial-arrow-down"></div>
						<div class="testimonial-author">
                        <? /*
							<div class="testimonial-author-thumbnail">
								<img src="../assets/images/team/team-1.jpg" class="img-responsive img-circle" alt="">
							</div>
                            */ ?>
							<p><?php echo $comment->username; ?><? /*<span>CEO & Founder - Okler</span></p>*/ ?>
						</div>
					</div>
                    <?php endforeach; ?>
				</div>
			</div>

		