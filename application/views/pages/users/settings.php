		<div class="body">
			<div role="main" class="main">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
                            <? $this->layout->show_message( $alert ); ?>
							<div class="tabs tabs-bottom tabs-center tabs-simple toggle">
								<ul class="nav nav-tabs">
									<li class="active">
										<a href="#tabsNavigationSimpleIcons1" data-toggle="tab">
											<span class="featured-boxes featured-boxes-style-6 p-none m-none">
												<span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
													<span class="box-content p-none m-none">
														<i class="icon-featured fa fa-user"></i>
													</span>
												</span>
											</span>									
											<p class="mb-none pb-none">Personal information</p>
										</a>
									</li>
									<li>
										<a href="#tabsNavigationSimpleIcons2" data-toggle="tab">
											<span class="featured-boxes featured-boxes-style-6 p-none m-none">
												<span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
													<span class="box-content p-none m-none">
														<i class="icon-featured fa fa-envelope"></i>
													</span>
												</span>
											</span>									
											<p class="mb-none pb-none">Еmail</p>
										</a>
									</li>
									<li>
										<a href="#tabsNavigationSimpleIcons3" data-toggle="tab">
											<span class="featured-boxes featured-boxes-style-6 p-none m-none">
												<span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
													<span class="box-content p-none m-none">
														<i class="icon-featured fa fa-key"></i>
													</span>
												</span>
											</span>									
											<p class="mb-none pb-none">Password</p>
										</a>
									</li>
									
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tabsNavigationSimpleIcons1">
										<div class="center">
											<h4>Personal Information</h4>
											<div class="col-sm-6 col-md-offset-3">
											<?php echo form_open('users/settings'); ?>
													<div class="row">
														<div class="form-group">
															<div class="col-md-12">
																<label>Username</label><?php echo form_error('username_new', '<span class="red"> *', '</span>'); ?>
																<input type="text" name="username_new" placeholder="<?php echo $user_info->username; ?>" class="form-control input-lg">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<div class="col-md-6">
																<label>First name</label> <?php echo form_error('first_name', '<span class="red"> *', '</span>'); ?>
																<input type="text" name="first_name" class="form-control input-lg" placeholder="<?php echo $user_info->first_name; ?>">
															</div>
															<div class="col-md-6">
																<label>Last name</label> <?php echo form_error('last_name', '<span class="red"> *', '</span>'); ?>
																<input type="text" name="last_name" class="form-control input-lg" placeholder="<?php echo $user_info->last_name; ?>">
															</div>
														</div>
													</div>
       
													<div class="row">
														<div class="col-md-12">
															<input type="submit" value="Save" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
														</div>
													</div>
												<?php echo form_close();?>
												</div>
										</div>
									</div>
										<div class="tab-pane" id="tabsNavigationSimpleIcons2">
										<div class="center">
											<h4>Email</h4>
											<div class="col-sm-6 col-md-offset-3">
											<?php echo form_open('users/set_email'); ?>
													<div class="row">
														<div class="form-group">
															<div class="col-md-12">
																<label>Current email</label><?php echo form_error('email', '<span class="red"> *', '</span>'); ?>
																<input type="email" name="email" class="form-control input-lg" placeholder="Current email..">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<div class="col-md-12">
																<label>New email</label><?php echo form_error('email_reg', '<span class="red"> *', '</span>'); ?>
																<input type="email" name="email_reg" class="form-control input-lg" placeholder="New email..">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<div class="col-md-12">
																<label>Confirm New email</label><?php echo form_error('emailconf', '<span class="red"> *', '</span>'); ?>
																<input type="email" name="emailconf" class="form-control input-lg" placeholder="New email..">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<input type="submit" value="Save" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
														</div>
													</div>
												<?php echo form_close();?>
												</div>
										</div>
									</div>
									<div class="tab-pane" id="tabsNavigationSimpleIcons3">
										<div class="center">
											<h4>Password</h4>
											<div class="col-sm-6 col-md-offset-3">
											<?php echo form_open('users/set_password');?>
													<div class="row">
														<div class="form-group">
															<div class="col-md-12">
																<label>Old password</label><?php echo form_error('password_current', '<span class="red"> *', '</span>'); ?>
																<input type="password" name="password_current" class="form-control input-lg" placeholder="Old password..">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<div class="col-md-6">
																<label>New password</label> <?php echo form_error('password', '<span class="red"> *', '</span>'); ?>
																<input type="password" name="password" class="form-control input-lg" placeholder="New password..">
															</div>
															<div class="col-md-6">
																<label>Confirm new password</label> <?php echo form_error('passconf', '<span class="red"> *', '</span>'); ?>
																<input type="password" name="passconf" class="form-control input-lg" placeholder="Confirm new password..">
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<input type="submit" value="Save" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
														</div>
													</div>
												<?php echo form_close();?>
												</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>