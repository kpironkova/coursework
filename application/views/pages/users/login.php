<div role="main" class="main">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="featured-boxes">
								<div class="row">
									<div class="col-sm-6 col-md-offset-3">
										<div class="featured-box featured-box-primary align-left mt-xlg">
											<div class="box-content login-box">
												<h4 class="heading-primary text-uppercase mb-md">Login</h4>
												<?php if (isset($alert)) $this->layout->show_message( $alert ); ?>
												<?php echo form_open('users/login', ['id' => 'frmSignUp']); ?>
													<div class="row">
														<div class="form-group">
															<div class="col-md-12">
																<label>E-mail Address</label>
																<input type="text" name="email" value="<?php echo set_value('email'); ?>" class="form-control input-lg" />
                                                                <?php echo form_error('email', '<span class="small red"> *', '</span>'); ?>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<div class="col-md-12">
																<a class="pull-right" href="#">(Forgot Password?)</a>
																<label>Password</label>
																<input type="password" name="password" class="form-control input-lg">
                                                                <?php echo form_error('password', '<span class="small red"> *', '</span>'); ?>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<input type="submit" value="Login" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
														</div>
													</div>
												<?php echo form_close();?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>