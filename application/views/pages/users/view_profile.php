<div class="container">
<div class="col-lg-12 col-sm-12">
    <div class="card hovercard">
        <div class="card-background">
            <img class="card-bkimg" alt="" src="http://lorempixel.com/100/100/people/9/">
            <!-- http://lorempixel.com/850/280/people/9/ -->
        </div>
        <div class="useravatar">
            <img alt="" src="http://lorempixel.com/100/100/people/9/">
        </div>
        <div class="card-info"> <span class="card-title"><?=text($user_info->first_name.' '.$user_info->last_name)?></span>

        </div>
    </div>
    <div class="btn-pref btn-group btn-group-justified btn-group-lg" role="group" aria-label="...">
        <div class="btn-group" role="group">
            <button type="button" id="statistics" class="btn btn-primary" href="#tab1" data-toggle="tab"><i class="fa fa-pie-chart fa-lg"></i>
                <div class="hidden-xs">Statistics</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="watched" class="btn btn-default" href="#tab2" data-toggle="tab"><i class="fa fa-film fa-lg"></i>
                <div class="hidden-xs">Watched</div>
            </button>
        </div>
        <div class="btn-group" role="group">
            <button type="button" id="reviews" class="btn btn-default" href="#tab3" data-toggle="tab"><i class="fa fa-comments-o fa-lg"></i>
                <div class="hidden-xs">Reviews</div>
            </button>
        </div>
    </div>

       
      <div class="tab-content">
        <div class="tab-pane fade in active" id="tab1">
          <h5>My movie maniac statistics</h5>
          <div class="row">
            <div class="col-md-2">
                 <div id="morris_donut"  style="height: 200px;"></div>
                 <h6 class="text-center">Watched genres</h6>
            </div>
            <div class="col-md-8">
                <div class="testimonial testimonial-style-1">
                    <blockquote>
          				<p>Congratulations! You wasted: <?= date('H:i', mktime(0, $wasted_time)); ?> hours watching movies!</p>
           			</blockquote>
                </div>
            </div>
          </div>
          </div>
        <div class="tab-pane fade in" id="tab2">
          
                <div class="row">
            			<ul class="portfolio-list">
                            <? foreach($movies as $movie): ?>
            				<li class="col-md-2 col-sm-3 col-xs-3 isotope-item websites">
            					<div class="portfolio-item">
            						<a href="<?=site_url('catalog/view/'.$movie['id'])?>">
            							<span class="thumb-info">
            								<span class="thumb-info-wrapper">
            									<img src="<?=base_url()?>assets/posters/<?=$movie['year']?>/165x245/<?=$movie['poster']?>" class="img-responsive" alt="">
            									<span class="thumb-info-title">
            										<span class="thumb-info-inner"><?=text($movie['title'])?></span>
                                                    <? /*
            										<span class="thumb-info-type">Website</span>
                                                    */ ?>
            									</span>
            									<span class="thumb-info-action">
            										<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
            									</span>
            								</span>
            							</span>
            						</a>
            					</div>
            				</li>
                            <? endforeach; ?>
            			</ul>
               
            </div>
          
        </div>
        <div class="tab-pane fade in" id="tab3">
          <h5>My reviews</h5>

            <?php foreach($comments as $comment): ?>
            <div class="row">
                <div class="col-xs-2">
    				<div class="portfolio-item">
    					<a href="<?=site_url('catalog/view/'.$comment->movie['id'])?>">
    						<span class="thumb-info">
    							<span class="thumb-info-wrapper">
    								<img src="<?=base_url()?>assets/posters/<?=$comment->movie['year']?>/165x245/<?=$comment->movie['poster']?>" class="img-responsive" alt="">
    								<span class="thumb-info-title">
    									<span class="thumb-info-inner"><?=text($comment->movie['title'])?></span>
                                        <? /*
    									<span class="thumb-info-type">Website</span>
                                        */ ?>
    								</span>
    								<span class="thumb-info-action">
    									<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
    								</span>
    							</span>
    						</span>
    					</a>
                    </div>
                </div>
                <div class="col-xs-10">
            		<div class="testimonial testimonial-style-1">
            			<blockquote>
            				<p><?= $comment->data; ?></p>
            			</blockquote>
            			<div class="testimonial-arrow-down"></div>
            			<div class="testimonial-author">
                        <? /*
            				<div class="testimonial-author-thumbnail">
            					<img src="../assets/images/team/team-1.jpg" class="img-responsive img-circle" alt="">
            				</div>
                            */ ?>
            				<p><?php echo $comment->username; ?><? /*<span>CEO & Founder - Okler</span></p>*/ ?>
            			</div>
            		</div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
      </div>
    
    </div>
            	</div>
    