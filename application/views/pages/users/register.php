		<div class="body">
			<div role="main" class="main">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="featured-boxes">
								<div class="row">
									<div class="col-sm-6 col-md-offset-3">
										<div class="featured-box featured-box-primary align-left mt-xlg">
											<div class="box-content">
												<h4 class="heading-primary text-uppercase mb-md">Register An Account</h4>
												<?php echo form_open('users/register', ['id' => 'frmSignUp']); ?>
													<div class="row">
														<div class="form-group">
															<div class="col-md-6">
																<label>First name</label>
																<input type="text" name="first_name" value="<?php echo set_value('first_name'); ?>" class="form-control input-lg">
                                                                <?php echo form_error('first_name', '<span class="small red"> *', '</span>'); ?>
															</div>
															<div class="col-md-6">
																<label>Last name</label>
																<input type="text" name="last_name" value="<?php echo set_value('last_name'); ?> "class="form-control input-lg">
                                                                <?php echo form_error('last_name', '<span class="small red"> *', '</span>'); ?>
															</div>
														</div>
													</div> 
													<div class="row">
														<div class="form-group">
															<div class="col-md-12">
																<label>Username</label>
																<input type="text" name="username_reg" value="<?php echo set_value('username_reg'); ?>" class="form-control input-lg" />
                                                                <?php echo form_error('username_reg', '<span class="small red"> *', '</span>'); ?>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<div class="col-md-12">
																<label>E-mail Address</label>
																<input type="text" name="email_reg" value="<?php echo set_value('email_reg'); ?>" class="form-control input-lg" />
                                                                <?php echo form_error('email_reg', '<span class="small red"> *', '</span>'); ?>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="form-group">
															<div class="col-md-6">
																<label>Password</label>
																<input type="password" name="password" class="form-control input-lg" />
                                                                <?php echo form_error('password', '<span class="small red"> *', '</span>'); ?>
															</div>
															<div class="col-md-6">
																<label>Confirm Password</label>
																<input type="password" name="passconf" class="form-control input-lg" />
                                                                <?php echo form_error('passconf', '<span class="small red"> *', '</span>'); ?>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-md-12">
															<input type="submit" value="Register" class="btn btn-primary pull-right mb-xl" data-loading-text="Loading...">
														</div>
													</div>
												<?php echo form_close();?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>