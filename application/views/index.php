<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>Porto - Responsive HTML5 Template 3.9.0</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url()?>assets/images/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php echo base_url()?>assets/images/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/bootstrap/bootstrap.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/fontawesome/css/font-awesome.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/owlcarousel/owl.carousel.min.css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/owlcarousel/owl.theme.default.min.css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/magnific-popup/magnific-popup.css" media="screen">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-blog.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-shop.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-animate.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/home_page.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/rs-plugin/css/settings.css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/circle-flip-slideshow/css/component.css" media="screen">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url()?>assets/vendor/modernizr/modernizr.js"></script>

		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="vendor/respond/respond.js"></script>
			<script src="vendor/excanvas/excanvas.js"></script>
		<![endif]-->

	</head>
	<body>
		<div class="body">
			<header id="header">
				<div class="container">
					<div class="logo">
						<a href="index.html">
							<img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" src="<?php echo base_url()?>assets/images/logo.png">
						</a>
					</div>
					<div class="search">
						<form id="searchForm" action="page-search-results.html" method="get">
							<div class="input-group">
								<input type="text" class="form-control search" name="q" id="q" placeholder="Search..." required>
								<span class="input-group-btn">
									<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
								</span>
							</div>
						</form>
					</div>
					<nav class="nav-top">
						<ul class="nav nav-pills nav-top">
							<li>
								<a href="<?php echo base_url();?>index.php/functions/register"><i class="fa fa-angle-right"></i>Sign up</a>
							</li>
							<li>
								<a href="<?php echo base_url();?>index.php/functions/login"><i class="fa fa-angle-right"></i>Log in</a>
							</li>
						</ul>
					</nav>
					<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="navbar-collapse nav-main-collapse collapse">
					<div class="container">
						<ul class="social-icons">
							<li class="facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook">Facebook</a></li>
							<li class="twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter">Twitter</a></li>
							<li class="linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin">Linkedin</a></li>
						</ul>
						<nav class="nav-main mega-menu">
							<ul class="nav nav-pills nav-main" id="mainMenu">
								<li class="dropdown active">
									<a class="home" href="index.html">
										Home
									</a>
								</li>
								<li class="dropdown mega-menu-item mega-menu-fullwidth">
									<a class="dropdown-toggle" href="#">
										Catalogues
									</a>
									<ul class="dropdown-menu">
										<li>
											<div class="mega-menu-content">
												<div class="row">
													<div class="col-md-3">
														<span class="mega-menu-sub-title">MOVIES</span>
														<ul class="sub-menu col-md-3">
															<li>
																<ul class="sub-menu">
																	<li><a href="shortcodes-accordions.html">All</a></li>
																	<li><a href="shortcodes-accordions.html">Action</a></li>
																	<li><a href="shortcodes-accordions.html">Adventure</a></li>
																	<li><a href="shortcodes-accordions.html">Comedy</a></li>
																	<li><a href="shortcodes-accordions.html">Crime</a></li>
																	<li><a href="shortcodes-accordions.html">Documentary</a></li>
																	<li><a href="shortcodes-accordions.html">Drama</a></li>
																</ul>
																<ul class="sub-menu">
																	<li><a href="shortcodes-accordions.html">Fantasy</a></li>
																	<li><a href="shortcodes-accordions.html">Horror</a></li>
																	<li><a href="shortcodes-accordions.html">Musical</a></li>
																	<li><a href="shortcodes-accordions.html">Psycho</a></li>
																	<li><a href="shortcodes-accordions.html">Thriller</a></li>
																	<li><a href="shortcodes-accordions.html">Sci-Fi</a></li>
																	<li><a href="shortcodes-accordions.html">Western</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<span class="mega-menu-sub-title">TV SERIES/SHOWS</span>
														<ul class="sub-menu">
															<li>
																<ul class="sub-menu">
																	<li><a href="shortcodes-buttons.html">Top Rated TV Shows</a></li>
																	<li><a href="shortcodes-buttons.html">Most Popular TV Shows</a></li>
																	<li><a href="shortcodes-buttons.html">DVD/Blu Ray</a></li>
																</ul>
															</li>
														</ul>
													</div>
													<div class="col-md-3">
														<span class="mega-menu-sub-title">ANIMATION/ANIME</span>
														<ul class="sub-menu">
															<li>
																<ul class="sub-menu">
																	<li><a href="shortcodes-call-to-action.html">Call to Action</a></li>
																</ul>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>	
								</li>
								<li class="watchlist">
									<a class="watchlist" href="index.html">
										Watchlist
									</a>
								</li>
							</ul>
						</nav>
					</div>
				</div>
			</header>

			<div role="main" class="main">
				<div class="slider-container">
					<div class="slider" id="revolutionSlider" data-plugin-revolution-slider data-plugin-options='{"startheight": 500}'>
						<ul>
							<li data-transition="fade" data-slotamount="13" data-masterspeed="300" >
								<iframe class="slide-show-video" src="https://player.vimeo.com/video/91160492"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							</li>
							<li data-transition="fade" data-slotamount="13" data-masterspeed="300" >
								<iframe class="slide-show-video" src="https://player.vimeo.com/video/140587787"  frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							</li>
						</ul>
					</div>
				</div>
				<div class="home-intro" id="home-intro">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<p>
									The fastest way to grow your business with the leader in <em>Technology</em>
									<span>Check out our options and features included.</span>
								</p>
							</div>
							<div class="col-md-4">
								<div class="get-started">
									<a href="#" class="btn btn-lg btn-primary">Watch more trailers</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="container">
					<div class="row center">
						<div class="col-md-12">
							<h1 class="mb-sm word-rotator-title">
								Best rated	
							</h1>
						</div>
					</div>
					<div class="row center">
						<div class="col-md-2 center">
							<img class="img-responsive poster-size" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
						</div>
						<div class="col-md-2">
							<img class="img-responsive poster-size" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
						</div>
						<div class="col-md-2">
							<img class="img-responsive poster-size" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
						</div>
						<div class="col-md-2">
							<img class="img-responsive poster-size" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
						</div>
						<div class="col-md-2">
							<img class="img-responsive poster-size" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
						</div>
						<div class="col-md-2">
							<img class="img-responsive poster-size" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
						</div>
								<div class="col-md-2 center">
							<img class="img-responsive poster-size" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
						</div>
						<div class="col-md-2">
							<img class="img-responsive poster-size" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
						</div>
						<div class="col-md-2">
							<img class="img-responsive poster-size" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
						</div>
						<div class="col-md-2">
							<img class="img-responsive poster-size" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
						</div>
						<div class="col-md-2">
							<img class="img-responsive poster-size" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
						</div>
						<div class="col-md-2">
							<img class="img-responsive poster-size" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
						</div>
					</div>
				</div>
				
				<div class="home-concept">
					<div class="container">
				
						<div class="row center">
						</div>
					</div>
				</div>


				<div class="container">
					<hr class="tall">
				
					<div class="row center">
						<div class="col-md-12">
							<h2 class="mb-sm word-rotator-title">
								Also see
							</h2>
						</div>
					</div>
				
					<div class="row center">
						<div class="owl-carousel" data-plugin-options='{"items": 6, "autoplay": true, "autoplayTimeout": 2000}'>
							<div>
								<img class="img-responsive slide-show-poster" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
							</div>
							<div>
								<img class="img-responsive slide-show-poster" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
							</div>
							<div>
								<img class="img-responsive slide-show-poster" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
							</div>
							<div>
								<img class="img-responsive slide-show-poster" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
							</div>
							<div>
								<img class="img-responsive slide-show-poster" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
							</div>
							<div>
								<img class="img-responsive slide-show-poster" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
							</div>
							<div>
								<img class="img-responsive slide-show-poster" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
							</div>
							<div>
								<img class="img-responsive slide-show-poster" src="<?php echo base_url()?>assets/images/maleficent.jpg" alt="">
							</div>
						</div>
					</div>
				</div>
 
				
				<section class="section section-custom-map">
					<section class="section section-default section-footer">
						<div class="container">
							<div class="row">
								<div class="col-md-6">
									<div class="recent-posts mb-xl">
										<h2>Latest <strong>Blog</strong> Posts</h2>
										<div class="row">
											<div class="owl-carousel mb-none" data-plugin-options='{"items": 1}'>
												<div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">15</span>
																<span class="month">Jan</span>
															</div>
															<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">15</span>
																<span class="month">Jan</span>
															</div>
															<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
												</div>
												<div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">12</span>
																<span class="month">Jan</span>
															</div>
															<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">11</span>
																<span class="month">Jan</span>
															</div>
															<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
												</div>
												<div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">15</span>
																<span class="month">Jan</span>
															</div>
															<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat libero. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
													<div class="col-md-6">
														<article>
															<div class="date">
																<span class="day">15</span>
																<span class="month">Jan</span>
															</div>
															<h4 class="heading-primary"><a href="blog-post.html">Lorem ipsum dolor</a></h4>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. <a href="/" class="read-more">read more <i class="fa fa-angle-right"></i></a></p>
														</article>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<h2><strong>What</strong> Client’s Say</h2>
									<div class="row">
										<div class="owl-carousel mb-none" data-plugin-options='{"items": 1}'>
											<div>
												<div class="col-md-12">
													<div class="testimonial testimonial-primary">
														<blockquote>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.  Donec hendrerit vehicula est, in consequat.  Donec hendrerit vehicula est, in consequat.</p>
														</blockquote>
														<div class="testimonial-arrow-down"></div>
														<div class="testimonial-author">
															<div class="testimonial-author-thumbnail img-thumbnail">
																<img src="img/clients/client-1.jpg" alt="">
															</div>
															<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
														</div>
													</div>
												</div>
											</div>
											<div>
												<div class="col-md-12">
													<div class="testimonial testimonial-primary">
														<blockquote>
															<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec hendrerit vehicula est, in consequat.</p>
														</blockquote>
														<div class="testimonial-arrow-down"></div>
														<div class="testimonial-author">
															<div class="testimonial-author-thumbnail img-thumbnail">
																<img src="img/clients/client-1.jpg" alt="">
															</div>
															<p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
				</section>
			</div>
 
			<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="footer-ribbon">
							<span>Get in Touch</span>
						</div>
						<div class="col-md-3">
							<div class="newsletter">
								<h4 class="heading-primary">Newsletter</h4>
								<p>Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.</p>
			
								<div class="alert alert-success hidden" id="newsletterSuccess">
									<strong>Success!</strong> You've been added to our email list.
								</div>
			
								<div class="alert alert-danger hidden" id="newsletterError"></div>
			
								<form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST">
									<div class="input-group">
										<input class="form-control" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">
										<span class="input-group-btn">
											<button class="btn btn-default" type="submit">Go!</button>
										</span>
									</div>
								</form>
							</div>
						</div>
						<div class="col-md-3">
							<h4 class="heading-primary">Latest Tweets</h4>
							<div id="tweet" class="twitter" data-plugin-tweets data-plugin-options='{"username": "", "count": 2}'>
								<p>Please wait...</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="contact-details">
								<h4 class="heading-primary">Contact Us</h4>
								<ul class="contact">
									<li><p><i class="fa fa-map-marker"></i> <strong>Address:</strong> 1234 Street Name, City Name, United States</p></li>
									<li><p><i class="fa fa-phone"></i> <strong>Phone:</strong> (123) 456-7890</p></li>
									<li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@example.com">mail@example.com</a></p></li>
								</ul>
							</div>
						</div>
						<div class="col-md-2">
							<h4 class="heading-primary">Follow Us</h4>
							<div class="social-icons">
								<ul class="social-icons">
									<li class="facebook"><a href="http://www.facebook.com/" target="_blank" data-placement="bottom" data-tooltip title="Facebook">Facebook</a></li>
									<li class="twitter"><a href="http://www.twitter.com/" target="_blank" data-placement="bottom" data-tooltip title="Twitter">Twitter</a></li>
									<li class="linkedin"><a href="http://www.linkedin.com/" target="_blank" data-placement="bottom" data-tooltip title="Linkedin">Linkedin</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<!--[if lt IE 9]>
		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="<?php echo base_url()?>assets/vendor/jquery/jquery.js"></script>
		<!--<![endif]-->
		<script src="<?php echo base_url()?>assets/vendor/jquery.appear/jquery.appear.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery.easing/jquery.easing.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery-cookie/jquery-cookie.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/bootstrap/bootstrap.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/common/common.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery.validation/jquery.validation.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery.stellar/jquery.stellar.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery.gmap/jquery.gmap.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/isotope/jquery.isotope.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/owlcarousel/owl.carousel.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jflickrfeed/jflickrfeed.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/vide/vide.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url()?>assets/js/theme.js"></script>
		
		<!-- Specific Page Vendor and Views -->
		<script src="<?php echo base_url()?>assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script>
		<script src="<?php echo base_url()?>assets/js/views/view.home.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url()?>assets/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url()?>assets/js/theme.init.js"></script>

		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script type="text/javascript">
		
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-12345678-1']);
			_gaq.push(['_trackPageview']);
		
			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		
		</script>
		 -->

	</body>
</html>