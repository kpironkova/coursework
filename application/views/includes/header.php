<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>Websterite</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="<?php echo base_url()?>assets/images/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php echo base_url()?>assets/images/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/bootstrap/bootstrap.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/fontawesome/css/font-awesome.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/owlcarousel/owl.carousel.min.css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/owlcarousel/owl.theme.default.min.css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/magnific-popup/magnific-popup.css" media="screen">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/theme.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-elements.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-blog.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-shop.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/theme-animate.css">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/home_page.css">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/morris.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/rs-plugin/css/settings.css" media="screen">
		<link rel="stylesheet" href="<?php echo base_url()?>assets/vendor/circle-flip-slideshow/css/component.css" media="screen">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/skins/default.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">

		<!-- Head Libs -->
		<script src="<?php echo base_url()?>assets/vendor/modernizr/modernizr.js"></script>

		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="vendor/respond/respond.js"></script>
			<script src="vendor/excanvas/excanvas.js"></script>
		<![endif]-->

	</head>
	<body class="dark">
		<div class="body">
			<header id="header">
				<div class="container">
					<div class="logo">
						<a href="<?php echo base_url(); ?>">
							<img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" src="<?php echo base_url()?>assets/images/logos/logo.png">
						</a>
					</div>
					<nav class="nav-top">
						<ul class="nav nav-pills nav-top">
							<?php if (isset($user_info->username)): ?>
								<div class="dropdown">
								  <button class="btn btn-primary dropdown" type="button" data-toggle="dropdown"><?php echo $user_info->username; ?>
								  <span class="caret"></span></button>
								  <ul class="dropdown-menu">
								    <li><a href="<?=site_url('users/view_profile')?>">View profile</a></li>
								    <li><a href="<?=site_url('users/settings')?>">Settings</a></li>
								    <li><a href="<?=site_url('users/logout')?>">Log out</a></li>
								  </ul> 
								</div>
							<?php else: ?>	
								<li>
									<a href="<?=site_url('users/register')?>"><i class="fa fa-angle-right"></i>Sign up</a>
								</li>
								<li>
									<a href="<?=site_url('users/login')?>"><i class="fa fa-angle-right"></i>Log in</a>
								</li>
							<?php endif; ?>
						</ul>
					</nav>
					<button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
						<i class="fa fa-bars"></i>
					</button>
				</div>
				<div class="navbar-collapse nav-main-collapse collapse">
					<div class="container">
						<nav class="nav-main mega-menu">
							<ul class="nav nav-pills nav-main" id="mainMenu">
								<li>
									<a href="<?php echo base_url();?>">
										Home
									</a>
								</li>
                                <li>
									<a href="<?=site_url('catalog')?>">
										Movies
									</a>
								</li>
                                <li>
									<a href="<?=site_url('catalog/tv')?>">
										TV Series
									</a>
								</li>
                                <li>
									<a href="<?=site_url('catalog')?>/?genre=18">
										Animations
									</a>
								</li>
                                <? if ( is_logged_in() ): ?>
                                
                                <li>
									<a href="<?=site_url('catalog/recommended')?>">
										Recommended
									</a>
								</li>
                                
								<li>
									<a  href="<?=site_url('watchlist')?>">
										Watchlist
									</a>
								</li>
                                
                                <? endif; ?>
                                
                                <?
                                /*
                                <? foreach($media_genres as $genre): ?>
								<li><a href="<?=site_url('catalog?genre='.$genre->id)?>"><?=text($genre->genre)?></a></li>
                                <? endforeach; ?>
                                */
                                ?>
							</ul>
						</nav>
					</div>
				</div>
			</header>