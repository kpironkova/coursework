			<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="footer-ribbon">
							<span>Get in Touch</span>
						</div>
    					<div class="col-md-4">
							<div class="newsletter">
								<h4 class="heading-primary">Websterite</h4>
								<p>Websterite is an interactive online catalog for movies and tv series with other funny entertainments.You'
                                re a movie maniac or kind of a tv series zombie ? Then just enjoy Websterite !</p>
							</div>
						</div>
						<div class="col-md-4">
							<h4 class="heading-primary">Follow Us</h4>
							<div class="social-icons">
                                <a href="http://facebook.com/"><i class="fa fa-facebook-square fa-2x"></i></a>
                                <a href="http://twitter.com/"><i class="fa fa-twitter-square fa-2x"></i></a>
                                <a href="http://plus.google.com/"><i class="fa fa-google-plus-square fa-2x"></i></a>
							</div>
						</div>
						<div class="col-md-4">
							<h4 class="heading-primary"></h4>
							<div id="tweet" class="twitter" data-plugin-tweets data-plugin-options='{"username": "", "count": 2}'>
								<p>Copyright &copy; 2016 Websterite.</p>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<!--[if lt IE 9]>
		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<![endif]-->
		<!--[if gte IE 9]><!-->
		<script src="<?php echo base_url()?>assets/vendor/jquery/jquery.js"></script>
		<!--<![endif]-->
		<script src="<?php echo base_url()?>assets/vendor/jquery.appear/jquery.appear.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery.easing/jquery.easing.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery-cookie/jquery-cookie.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/bootstrap/bootstrap.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/common/common.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery.validation/jquery.validation.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery.stellar/jquery.stellar.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jquery.gmap/jquery.gmap.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/isotope/jquery.isotope.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/owlcarousel/owl.carousel.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/jflickrfeed/jflickrfeed.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/vide/vide.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url()?>assets/js/theme.js"></script>
		
		<!-- Specific Page Vendor and Views -->
		<script src="<?php echo base_url()?>assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
		<script src="<?php echo base_url()?>assets/vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script>
		<script src="<?php echo base_url()?>assets/js/views/view.home.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url()?>assets/js/custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url()?>assets/js/theme.init.js"></script>
        
        
        <? if(isset($chart_data)): ?>
        <!-- Morris chart -->
        <script src="<?php echo base_url()?>assets/js/raphael-min.js"></script>
        <script src="<?php echo base_url()?>assets/js/morris.min.js"></script>
        <script>
            Morris.Donut({
                element: 'morris_donut',
                data: [
                    <? foreach($chart_data as $index => $genre): ?>
                    {label: "<?=$genre->genre?>", value: <?=$genre->percent?>} <? if(isset($chart_data[$index+1])) echo ",\n"; ?>
                    <? endforeach; ?>
                ],
                formatter: function (y) { return y + "%" }
            });
        </script>
       	<? endif; ?>
	</body>
</html>