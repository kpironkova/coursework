<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Watchlist extends CI_Controller
{
    public $data;

    public function index()
    {
         if ( is_logged_in() ){        
             $this->data['movies'] = [];
             $watchlist_movies = $this->watchlist_model->get_all();

            foreach($watchlist_movies as $movie) {
                $this->data['movies'][] = [
                    'id'            => $movie->media_id,
                    'date_added'    => $movie->date_added,
                    'title'         => $this->media_model->get_title($movie->media_id),
                    'year'         => $this->media_model->get_year($movie->media_id),
                    'poster'        => $this->media_model->get_poster($movie->media_id),
                ];
            }
             
             $this->layout->render('watchlist/listing');
         }else $this->layout->render('users/login');
    }
    
    
    public function add_to_watchlist($media_id) {
        if (is_logged_in()){
             $this->watchlist_model->add_to_watchlist($media_id);
             $this->session->set_flashdata( 'alert', ['success', 'Added to watchlist!'] );
             redirect('catalog/view/' . $media_id);
        }else $this->layout->render('users/login');
       
    }
    
    public function delete_from_watchlist($media_id) {
        $this->watchlist_model->delete_from_watchlist($media_id);
        $this->session->set_flashdata( 'alert', ['success', 'Removed from watchlist!'] );
        redirect('catalog/view/' . $media_id);
    }

}
