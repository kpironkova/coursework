<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Catalog extends CI_Controller
{
    public $data;

    public function index()
    {
        $this->media_model->set_type(1);

        /** Филтрираме по жанр **/

        $genre_id = (int)$this->input->get('genre');
        if ($genre_id != 0) {
            if (!$this->media_model->exists_genre($genre_id)) {
                $this->data['alert'] = ['danger', 'Unknown genre!'];
                $this->layout->render('catalog/error');
                return;

                // Проверяваме дали все пак има такъв жанр и ако няма бием грешка и ретърн, за да не се изпълни кода надолу.

            } else
                $this->media_model->set_genre($genre_id);
        }

        /** Филтрираме по година **/

        $year = (int)$this->input->get('year');
        if ($year != 0) {
            if ($year < 1900 || $year >= date('Y')) {
                $this->data['alert'] = ['danger', 'Unknown year!'];
                $this->layout->render('catalog/error');
                return;

                // Кратка проверка за годината.

            } else
                $this->media_model->set_year($year);
        }

        /** Взимаме филмите **/

        $this->data['movies'] = $this->media_model->get_all();

        /** Темплейт **/

        $this->layout->render('catalog/listing');
    }

    public function tv()
    {
        $this->media_model->set_type(2);
        
        /** Филтрираме по жанр **/
                
        $genre_id = (int)$this->input->get('genre');
        if ($genre_id != 0) {
            if (!$this->media_model->exists_genre($genre_id)) {
                $this->data['alert'] = ['danger', 'Unknown genre!'];
                $this->layout->render('catalog/error');
                return;

                // Проверяваме дали все пак има такъв жанр и ако няма бием грешка и ретърн, за да не се изпълни кода надолу.

            } else
                $this->media_model->set_genre($genre_id);
        }

        /** Филтрираме по година **/

        $year = (int)$this->input->get('year');
        if ($year != 0) {
            if ($year < 1900 || $year >= date('Y')) {
                $this->data['alert'] = ['danger', 'Unknown year!'];
                $this->layout->render('catalog/error');
                return;

                // Кратка проверка за годината.

            } else
                $this->media_model->set_year($year);
        }

        /** Взимаме филмите **/

        $this->data['movies'] = $this->media_model->get_all();

        /** Темплейт **/

        $this->layout->render('catalog/listing');
    }
    
    public function recommended()
    {
        if ( !is_logged_in() )
            redirect();
            
        $this->data['movies'] = $this->media_model->get_related_movies_all(36);
        
        $this->layout->render('catalog/recommended');
    }

    public function view($id)
    {
        $id = (int)$id;

        if ($id == 0 || !$this->media_model->exists_media($id)) {
            $this->data['alert'] = ['danger', 'Unknown movie!'];
            $this->layout->render('catalog/error');
            return;
        }
        
        if(isset($_POST['submit_comment']))
        {
            $this->_post_comment($id);
        }
        
        if (isset($_POST['vote'])) {
            $this->_vote_movie($id);
        }
        
       // var_dump($this->media_model->get_related_movies($id));

        $this->data['movie'] = $this->media_model->get_movie($id);
        $this->data['rating'] = $this->rating_model->check_rated($id);
        $this->data['total_rating'] = $this->rating_model->get_avg_rate($id);
        $this->data['vote_count'] = $this->rating_model->get_vote_count($id);
        $this->data['comments'] = $comments = $this->comments_model->get_comments($id);
        $this->data['comments_count'] = count($comments);
        $this->data['in_watchlist'] = $this->watchlist_model->exist_in_watchlist($id);
        $this->data['in_watched'] = $this->watched_model->exist_in_watched($id);
        $this->data['links'] = $this->media_model->get_links($id);
        
        $this->data['related_movies'] = $this->media_model->get_related_movies($id, 6);
        
        $this->layout->render('catalog/view-movie');
    }
    
    /** PRIVATE **/
    
    private function _post_comment($media_id)
    {
        if (is_logged_in()){
             if ($this->form_validation->run('comments/post_comment') !== false) {
                $this->comments_model->post_comment($media_id);
             }
        } else {
            redirect('users/login');
        }
       
    }
    
    private function _vote_movie($id) {
            if (is_logged_in()) {
                if ($this->rating_model->check_rated($id) == null) {
                    if ($this->input->post('rating')) {
                        $this->rating_model->set_rating($id, $this->input->post('rating'));
                        $this->session->set_flashdata( 'alert', ['success', 'Movie rated successfully!'] );
                        redirect('catalog/view/' . $id);
                    } else {
                     $this->session->set_flashdata( 'alert', ['danger', 'Please select a star'] );
                     redirect('catalog/view/' . $id);                         
                    }
                } else {
                     $this->session->set_flashdata( 'alert', ['danger', 'You have already rated that movie'] );
                     redirect('catalog/view/' . $id);                    
                }
            } else {
                 $this->session->set_flashdata( 'alert', ['danger', 'You have to login before rate that movie'] );
                 redirect('catalog/view/' . $id);
            }        
    }
            
}
