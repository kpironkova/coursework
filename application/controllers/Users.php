<?php

defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Users extends CI_Controller{
    public $data;

    public function register(){
        if ( is_logged_in() )
            redirect();

        if ( $this->form_validation->run() !== false ){
            //call user_model -> save username,email,password
            $this->user_model->register();
            // save in session success message
            $this->session->set_flashdata( 'alert', ['success', 'Register successfully! You can now log in.'] );
            // redirect -> green baloon .. "successfully registrated"
            redirect( 'users/login' );
        }

        $this->layout->render( 'users/register' );
    }

    public function login(){
        if ( is_logged_in() )
            redirect();

        $this->data['alert'] = '';

        if ( $this->form_validation->run() !== false ){
            if ( ! $this->user_model->login() )
                $this->data['alert'] = ['danger', 'Wrong email or password!'];
            else
                redirect();
        }

        $this->layout->render( 'users/login' );
    }

    public function logout(){
        $this->session->sess_destroy();

        redirect();
    }


    public function settings(){
        if ( ! is_logged_in() )
            redirect();

        $this->data['alert'] = '';
        
        if ( $this->form_validation->run() !== false ){
            
            if ( ! empty( $this->input->post( 'username_new' ) ) || 
                    ! empty( $this->input->post( 'first_name' ) ) || 
                    ! empty( $this->input->post( 'last_name' ) ) 
            ){
                $this->user_model->set_personal_info();
                
                $this->data['alert'] = ['success', 'Your changes are successfully saved!'];
            } else
                $this->data['alert'] = ['success', 'All fields are empty!'];
        }

       $this->layout->render( 'users/settings' );
    }

    public function set_email(){
        if ( ! is_logged_in() )
            redirect();

        $this->data['alert'] = '';

        if ( $this->form_validation->run() !== false ){
            if ( $this->input->post( 'email' ) == $this->user_model->get_info()->email ){
                $this->user_model->set_email();
                $this->data['alert'] = ['success', 'Your changes are successfully saved!'];
            } else
                $this->data['alert'] = ['success', 'Wrong current email!'];
        }

        $this->layout->render( 'users/settings' );
    }

    public function set_password(){
        if ( ! is_logged_in() )
            redirect();

        $this->data['alert'] = '';

        if ( $this->form_validation->run() !== false ){
            if ( $this->user_model->check_password() == true ){
                $this->user_model->set_password();
                $this->data['alert'] = ['success', 'Your changes are successfully saved!'];
            } else
                $this->data['alert'] = ['success', 'Wrong old password!'];
        }

        $this->layout->render('users/settings');
    }
    
    public function view_profile(){
        if ( !is_logged_in() )
            redirect();
            
        $this->data['movies'] = [];
        $watched_movies = $this->watched_model->get_all();

        foreach($watched_movies as $movie) {
            $this->data['movies'][] = [
                'id'            => $movie->media_id,
                'date_watched'    => $movie->date_watched,
                'title'         => $this->media_model->get_title($movie->media_id),
                'year'         => $this->media_model->get_year($movie->media_id),
                'poster'        => $this->media_model->get_poster($movie->media_id),
            ];
        } 
        
        $this->data['comments'] = $this->comments_model->get_comments_by_user_id($this->data['user_info']->id);
        foreach($this->data['comments'] as $index => $comment) {
            $this->data['comments'][$index]->movie = [
                'id'            => $comment->media_id,
                'title'         => $this->media_model->get_title($comment->media_id),
                'year'         => $this->media_model->get_year($comment->media_id),
                'poster'        => $this->media_model->get_poster($comment->media_id),
            ];
        } 
        
        $this->data['chart_data'] = $this->watched_model->get_chart_data();
        
        $this->data['wasted_time'] = $this->watched_model->sum_watched_time();

        $this->layout->render('users/view_profile');    
    }
}
