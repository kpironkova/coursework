<?php

defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Rss extends CI_Controller
{
    public function zamunda($cat)
    {
        if(!$cat)
            return;
            
        $categories[46] = '3D';
        $categories[20] = 'DVD-R';
        $categories[19] = 'SD';
        $categories[5] = 'HD';
        
        $this->load->model('crawler_model', 'crawler');
        
        $this->load->library( 'rssparser' );
        $this->rssparser->set_feed_url( 'http://zamunda.net/rss.php?cat='.$cat ); 
        $this->rssparser->set_cache_life( 5 ); 
        $rss = $this->rssparser->getFeed( 25 ); 


        $i = 0;
        foreach ( $rss as $item )
        {
            $title = $this->crawler->zamunda_parse_title( $item['title'] );

            if ( ! $title )
                continue;
            
            $size = $this->crawler->zamunda_parse_size( $item['description'] );
            
            $movies[$i] = $this->crawler->get_movie_info( $title[1], $title[2] );

            if ( 'True' != $movies[$i]->Response ) 
            {
                unset($movies[$i]);
                continue;
            }
            
            $movies[$i]->Size = $size;
            $movies[$i]->Link = $item['link'];
            $movies[$i]->LinkType = $categories[$cat];
            
            if ( property_exists( $movies[$i], 'Genre' ) )
            {
                $movies[$i]->Genre = explode( ',', $movies[$i]->Genre );
                foreach ( $movies[$i]->Genre as &$genre )
                    $genre = trim( $genre );
            }

            $i++;
        }
        
        $this->crawler->insert($movies);        
    }
    
    public function zamunda_tv($cat)
    {
        if(!$cat)
            return;
            
        $categories[7] = 'TV Shows';
        
        $this->load->model('crawler_model', 'crawler');
        
        $this->load->library( 'rssparser' );
        $this->rssparser->set_feed_url( 'http://zamunda.net/rss.php?cat='.$cat ); 
        $this->rssparser->set_cache_life( 5 ); 
        $rss = $this->rssparser->getFeed( 25 ); 



        $i = 0;
        foreach ( $rss as $item )
        {
            $title = $this->crawler->zamunda_parse_title( $item['title'], true );
            
            if ( ! $title )
                continue;
            
            $size = $this->crawler->zamunda_parse_size( $item['description'] );
            
            $movies[$i] = $this->crawler->get_tv_info( $title[1], array($title[2], $title[3]) );

            if ( 'True' != $movies[$i]->Response )
            {
                unset($movies[$i]);
                continue;
            }
            
            $movies[$i]->Size = $size;
            $movies[$i]->Link = $item['link'];
            $movies[$i]->LinkType = 'S'.$title[2].'E'.$title[3];
            
            if ( property_exists( $movies[$i], 'Genre' ) )
            {
                $movies[$i]->Genre = explode( ',', $movies[$i]->Genre );
                foreach ( $movies[$i]->Genre as &$genre )
                    $genre = trim( $genre );
            }

            $i++;
        }
        
        $this->crawler->insert($movies, true);        
    }

}
