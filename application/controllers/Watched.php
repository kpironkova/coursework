<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Watched extends CI_Controller
{
    public $data;
    
    public function index(){
          if ( is_logged_in() ){        
             echo "Marked as watched";
             $watched_movies = $this->watched_model->get_all();
          }else $this->layout->render('users/login');
    }
    
     public function add_to_watched($media_id) {
        if (is_logged_in()){
             $this->watched_model->add_to_watched($media_id);
             $this->session->set_flashdata( 'alert', ['success', 'Marked as watched!'] );
             redirect('catalog/view/' . $media_id);
        }else $this->layout->render('users/login');
       
    }
    
    public function delete_from_watched($media_id) {
        $this->watched_model->delete_from_watched($media_id);
        $this->session->set_flashdata( 'alert', ['success', 'Unmarked as watched'] );
        redirect('catalog/view/' . $media_id);
    }
}