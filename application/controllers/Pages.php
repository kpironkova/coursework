<?php

defined( 'BASEPATH' ) or exit( 'No direct script access allowed' );

class Pages extends CI_Controller
{
    public $data;

    public function home()
    {
        $this->data['latest_movies'] = $this->media_model->get_all(12);
        $this->data['best_rated_movies'] = $this->rating_model->get_best_rated(12);
        
        //echo '<pre>';
        //var_dump($this->rating_model->get_best_rated());
        
        $this->layout->render( 'home' );    
    }
}
